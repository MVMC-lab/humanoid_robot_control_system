import sys
import glob
import serial
from . import com
from PyQt5.QtWidgets import QMainWindow, QApplication
from .ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        avalible_baudrate = [
            str(9600), 
            str(38400),
            str(115200)
        ]
        self.setupUi(self)
        self.pushButton_Update.clicked.connect(self.update_com)
        self.pushButton_Connect.clicked.connect(self.connect)
        self.pushButton_Stop.clicked.connect(self.disconnect)
        self.pushButton_Send.clicked.connect(self.send_message)
        self.pushButton_Clear_mess.clicked.connect(self.clear_message)

        self.comboBox_Comport.activated.connect(self.choose_com)
        self.comboBox_BAUD.addItems(avalible_baudrate)
        self.comboBox_BAUD.activated.connect(self.choose_baud)
        
        self.lineEdit_Send.setReadOnly(True)

        self.label_status.setText(
            '目前串列埠: ---' + 
            self.comboBox_Comport.currentText() +
            ', 目前鮑率: ' +
            self.comboBox_BAUD.currentText()
        )

    def connect(self):
        if not 'COM' in self.label_status.text():
            self.textBrowser_Message.append('Error')
        else:
            self.textBrowser_Message.append(
                '--Connect.' + 
                self.comboBox_Comport.currentText() + 
                '--'
            )
            th = com.SerialRecv(port = self.comboBox_Comport.currentText())
            self.textBrowser_Message.append('Tsee')

            
    def disconnect(self):
        pass

    def send_message(self):
        pass
        
    def clear_message(self):
        self.textBrowser_Message.clear()

    def update_com(self):
        avalible = []
        ports = ['COM%s' % (i + 1) for i in range(256)]
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                avalible.append(port)
            except (OSError, serial.SerialException):
                pass
        
        self.comboBox_Comport.clear()
        self.comboBox_Comport.addItems(avalible)

    def choose_com(self):
        text_new = self.label_status.text().split(', ')[0].split(': ')[0]
        text_new += ': ' + self.comboBox_Comport.currentText() + ', '
        self.label_status.setText(
            text_new + self.label_status.text().split(', ')[1])

    def choose_baud(self):
        text_new = self.label_status.text().split(', ')[1].split(': ')[0]
        text_new += ': ' + self.comboBox_BAUD.currentText()
        self.label_status.setText(
            self.label_status.text().split(', ')[0] + ', ' + text_new)
    

