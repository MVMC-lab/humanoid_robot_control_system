from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal, QObject
import serial
import serial.tools.list_ports
import time

# Port: Com port nunber or "comX"
# Rx: Output variable
def Port_Read(Port, Baud, length=10, timeout=0.5):
    ser = serial.Serial(
        port = Port , 
        baudrate = Baud,
        timeout = 0.5
    )
    Rx = ser.read(size = length)
    # print(f'Recv: {Rx}')
    return Rx


def Port_Write(Port, Baud, Tx, timeout=0.5):
    ser = serial.Serial(
        port = Port, 
        baudrate = Baud, 
        timeout = 0.5
    )
    Tx = str(Tx)
    print(f'{ser.name} Send:{Tx}')
    Tx = bytes(Tx, encoding='utf-8') 
    Tx = ser.write(Tx)

# print(ser.isOpen())
# 是讀一行，以/n結束，要是沒有/n就一直讀，阻塞。
# data = ser.readline()

class SerialTrans(QThread):
    serdata = pyqtSignal(str)
    Tx = 0
    def __init__(self, parent=None, port='COM0', Tx = 0):
        super(SerialTrans, self).__init__()
        self.com = serial.Serial()
        self.com.port = port
        self.com.open()

    def run(self):
        print('Tx--IN')
        if self.com.out_waiting:
            self.com.reset_output_buffer()
        string = str(Tx)
        self.com.write(Tx)
        print(f'Send: {string}')


class SerialRecv(QThread):
    serdata = pyqtSignal(str)

    def __init__(self, parent=None, port='COM0'):
        super(SerialRecv, self).__init__()
        self.com = serial.Serial()
        self.com.port = port
        self.com.open()

    def run(self, intervaltime=1):
        print('Rx--IN')
        time.sleep(int(intervaltime))
        if self.com.in_waiting:
            buf = self.com.read(self.com.in_waiting)
            string = str(buf)
            self.serdata.emit(string)
            print(f'{string}')
            # return string
        else:
            print('Nothing in Buffer')
            # return '0'
        
