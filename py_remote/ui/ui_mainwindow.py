# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label_BAUD = QtWidgets.QLabel(self.centralwidget)
        self.label_BAUD.setGeometry(QtCore.QRect(60, 90, 35, 9))
        self.label_BAUD.setObjectName("label_BAUD")
        self.pushButton_Connect = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Connect.setGeometry(QtCore.QRect(290, 20, 51, 31))
        self.pushButton_Connect.setObjectName("pushButton_Connect")
        self.pushButton_Stop = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Stop.setGeometry(QtCore.QRect(290, 70, 51, 31))
        self.pushButton_Stop.setObjectName("pushButton_Stop")
        self.lineEdit_Send = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_Send.setGeometry(QtCore.QRect(50, 310, 113, 20))
        self.lineEdit_Send.setObjectName("lineEdit_Send")
        self.pushButton_Send = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Send.setGeometry(QtCore.QRect(180, 310, 61, 21))
        self.pushButton_Send.setObjectName("pushButton_Send")
        self.label_status = QtWidgets.QLabel(self.centralwidget)
        self.label_status.setGeometry(QtCore.QRect(60, 110, 281, 16))
        self.label_status.setObjectName("label_status")
        self.label_Com = QtWidgets.QLabel(self.centralwidget)
        self.label_Com.setGeometry(QtCore.QRect(60, 10, 71, 16))
        self.label_Com.setObjectName("label_Com")
        self.comboBox_Comport = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Comport.setGeometry(QtCore.QRect(140, 10, 71, 21))
        self.comboBox_Comport.setObjectName("comboBox_Comport")
        self.comboBox_BAUD = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_BAUD.setGeometry(QtCore.QRect(190, 80, 71, 21))
        self.comboBox_BAUD.setObjectName("comboBox_BAUD")
        self.pushButton_Update = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Update.setGeometry(QtCore.QRect(140, 40, 71, 20))
        self.pushButton_Update.setObjectName("pushButton_Update")
        self.textBrowser_Message = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser_Message.setGeometry(QtCore.QRect(50, 130, 281, 151))
        self.textBrowser_Message.setObjectName("textBrowser_Message")
        self.pushButton_Clear_mess = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_Clear_mess.setGeometry(QtCore.QRect(260, 310, 61, 21))
        self.pushButton_Clear_mess.setObjectName("pushButton_Clear_mess")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 17))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ComPorter"))
        self.label_BAUD.setText(_translate("MainWindow", "鮑率"))
        self.pushButton_Connect.setText(_translate("MainWindow", "Start"))
        self.pushButton_Stop.setText(_translate("MainWindow", "Stop"))
        self.pushButton_Send.setText(_translate("MainWindow", "Send"))
        self.label_status.setText(_translate("MainWindow", "狀態顯示"))
        self.label_Com.setText(_translate("MainWindow", "選擇Comport"))
        self.pushButton_Update.setText(_translate("MainWindow", "更新串列埠"))
        self.pushButton_Clear_mess.setText(_translate("MainWindow", "Clear"))
