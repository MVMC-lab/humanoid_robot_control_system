import os
import sys
# Expand sys.path with package source.
_ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_ROOT_DIR)

# NOTE: vscode 插件顯示路徑問題 
from ui.window import MainWindow
from PyQt5.QtWidgets import QMainWindow, QApplication

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
