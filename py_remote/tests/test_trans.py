import os
import sys
from time import sleep
# Expand sys.path with package source.
_ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_ROOT_DIR)

import comport.com as com

if __name__ == "__main__":
    print("Trans, START!!")
    Text = 0
    Port = 'COM10'
    Baud = 9600
    while(True):
        com.Port_Write(Port, Baud, Text)
        Text += 1
        sleep(0.5)
