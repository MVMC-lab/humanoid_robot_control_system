from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal, QObject
import time
import serial
import serial.tools.list_ports


class SerialHandler(QThread):
    signal_serialdata = pyqtSignal(str)

    def __init__(self):
        super(SerialHandler, self).__init__()
        self.com = serial.Serial()
        self.com.port = 'COM10'
        self.com.open()
    # override run function

    def run(self):
        while True:
            time.sleep(1)
            if self.com.in_waiting:
                buf = self.com.read(self.com.in_waiting)
                string = str(buf)
                self.signal_serialdata.emit(string)
                self.com.write(buf)
                print(f'{string}')
            else:
                print('Nothing in Buffer')

    def __del__(self):
        if self.com.is_open:
            self.com.close()
            print('Close Port ['+self.com.port+']')


class cli_print(QThread):
    def __init__(self):
        super(cli_print, self).__init__()


if __name__ == "__main__":
    COM = SerialHandler()
    COM.start()
    COM.wait()
