from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal, QObject
import time


class A(QObject):
    signal_1 = pyqtSignal()
    signal_2 = pyqtSignal(str)

    def __init__(self):
        super(A, self).__init__()

    def fun_1(self):
        self.signal_1.emit()

    def fun_2(self):
        self.signal_2.emit('QQ')


class B(QThread):
    
    def __init__(self):
        super(B, self).__init__()

    def fun_1(self):
        print('fun_1')

    def fun_2(self, msg):
        print(f'fun_2, msg is {msg}')

    def run(self):
        while True:
            pass


def run():
    a = A()
    b = B()
    a.signal_1.connect(b.fun_1)
    a.signal_2.connect(b.fun_2)

    b.start()

    time.sleep(1)
    a.signal_1.emit()

    time.sleep(1)
    a.signal_2.emit('QQ')

    time.sleep(1)
    a.signal_2.emit('AA')

if __name__ == "__main__":
    run()