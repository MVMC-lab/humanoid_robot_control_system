import os
import sys
# Expand sys.path with package source.
_ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_ROOT_DIR)

import comport.com as com

if __name__ == "__main__":
    print("Recv, START!!")
    Port = 'COM9'
    Baud = 9600
    while(True):
        com.Port_Read(Port, Baud)
